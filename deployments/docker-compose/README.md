
copy the configuration file to the configs directory before starting the service.

```
├── configs
│         └── edusys.yml
└── docker-compose.yml
```

running service:

> docker-compose up -d
